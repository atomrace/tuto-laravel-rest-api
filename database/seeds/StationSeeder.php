<?php

use Illuminate\Database\Seeder;

class StationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stations')->insert([
            'name' => '924 Gilman station',
            'long' => -122.30,
            'lat' => 37.88,
            'user_id' => 1
        ]);

        DB::table('stations')->insert([
            'name' => 'Broomfield station',
            'description' => 'CO2 station',
            'long' => -3.11,
            'lat' => 51.10,
            'user_id' => 1
        ]);

        DB::table('stations')->insert([
            'name' => 'Cégep de Ste-Foy station',
            'description' => 'G-266 station',
            'long' => -71.29,
            'lat' => 46.78,
            'user_id' => 2
            ]);
    }
}
