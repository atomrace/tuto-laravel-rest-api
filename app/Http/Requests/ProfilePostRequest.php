<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfilePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ddn' => 'filled|date',
            'web_site_url' => 'filled|string',
            'facebook_url' => 'filled|string',
            'linkedin_url' => 'filled|string'
        ];
    }
}
